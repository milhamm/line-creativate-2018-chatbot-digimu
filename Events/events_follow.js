import template_jenisKelamin from "../FlexMessage/template_jenisKelamin";

export function follow(replyToken, source, client, db) {
    const idUser = source.userId;

    if (idUser) {
        return client.getProfile(idUser)
            .then(profile => {
                const getDoc = db.collection('users').doc(profile.userId).get()
                    .then(doc => {
                        if (!doc.exists) {
                            console.log('User Not Exist');
                            const dbRef = db.collection('users').doc(profile.userId);
                            const setUser = dbRef.set({
                                'uid': profile.userId,
                                'displayName': profile.displayName,
                                'fJenisKelamin': 1,
                                'fUmur': 1,
                            });
                            callReplyMessage(profile, true);
                        } else {
                            console.log('User is Exist');
                            callReplyMessage(profile, false)
                        }
                    }).catch(err => console.log(err));
            }).catch(err => console.log("Error getting document:", err));
    } else {
        return client.replyMessage(replyToken, `Bot can't use profile API without user ID`);
    }

    function callReplyMessage(profile, userBaru) {
        let textReplyMessage = [{
            type: 'text',
            text: `Halo 👋 , terima kasih sudah menambahkan bot Digimu sebagai teman. Yuk ! kepoin aku, aku akan mengenalkanmu dengan museum-museum yang ada di Indonesia, selamat menjelajah 😁 ...`,
        }, {
            type: "sticker",
            packageId: "1",
            stickerId: "430"
        }];
        let infoJenisKelamin = [{
            type: "text",
            text: "Eh, Tapi Sebelumnya Kenalan Dulu Ya! 😅. Kakak Cowok Apa Cewek Nih ? "
        }, template_jenisKelamin()];
        if (userBaru) {
            textReplyMessage = [...textReplyMessage, ...infoJenisKelamin];
            client.replyMessage(replyToken, textReplyMessage).catch(err => {
                console.log("Error Replying message", err);
            })
        } else {
            client.replyMessage(replyToken, textReplyMessage).catch(err => {
                console.log("Error Replying message", err);
            })
        }

    }
}