import template_shareLokasi from "../FlexMessage/template_shareLokasi";
import template_updateLokasi from "../FlexMessage/template_updateLokasi";
import imagemap_menu from "../FlexMessage/imagemap_menu";

let profileId = '';
export async function handleText(message, replyToken, source, timestamp, client, db) {

    function replyTemplate(object) {
        return JSON.parse(JSON.stringify(object))
    }
    try {
        const idUser = source.userId;
        let profile = await client.getProfile(idUser);
        profileId = profile.userId;
        const dbRefB = db.collection('users').doc(profileId);
        let flagKelamin, flagUmur;
        switch (message.text.toLowerCase()) {
            case 'cowok':
                flagKelamin = await dbRefB.get();
                if (flagKelamin.data().fJenisKelamin === 1) {
                    await dbRefB.set({
                        'jenis_kelamin': 'cowok',
                        'fJenisKelamin': 0,
                    }, {merge: true});
                    await client.replyMessage(replyToken, JSON.parse(JSON.stringify({
                        "type": "text",
                        "text": "Woke!! 👍🏻 , kalau boleh tau mas umurnya Berapa Nih ?"
                    })));
                }
                break;
            case 'cewek':
                flagKelamin = await dbRefB.get();
                if (flagKelamin.data().fJenisKelamin === 1) {
                    await dbRefB.set({
                        'jenis_kelamin': 'cewek',
                        'fJenisKelamin': 0
                    }, {merge: true});
                    await client.replyMessage(replyToken, {
                        type: "text",
                        text: "Woke!! 👍🏻 , kalau boleh tau mbak umurnya Berapa Nih ?",
                    });
                }
                break;
            case 'menu':
                await client.replyMessage(replyToken, imagemap_menu());
                break;
            case 'cari museum':
                await client.replyMessage(replyToken, replyTemplate({
                    "type": "flex",
                    "altText": "Flex Message",
                    "contents": {
                        "type": "carousel",
                        "contents": [
                            {
                                "type": "bubble",
                                "direction": "ltr",
                                "hero": {
                                    "type": "image",
                                    "url": "https://firebasestorage.googleapis.com/v0/b/digimu-31cdd.appspot.com/o/Chatbot_Digimu%2Fassets%2FCari%20Museum-1.png?alt=media&token=2473c26a-4164-4728-a8db-7b7d1193fbc7",
                                    "margin": "none",
                                    "align": "center",
                                    "size": "full",
                                    "aspectRatio": "20:13",
                                    "aspectMode": "fit",
                                    "backgroundColor": "#FFFFFF"
                                },
                                "body": {
                                    "type": "box",
                                    "layout": "vertical",
                                    "spacing": "none",
                                    "margin": "none",
                                    "contents": [
                                        {
                                            "type": "text",
                                            "text": "Telusuri Museum Berdasarkan Nama ",
                                            "size": "lg",
                                            "align": "center",
                                            "gravity": "top",
                                            "weight": "regular",
                                            "wrap": true
                                        }
                                    ]
                                },
                                "footer": {
                                    "type": "box",
                                    "layout": "horizontal",
                                    "contents": [
                                        {
                                            "type": "button",
                                            "action": {
                                                "type": "message",
                                                "label": "Telusuri",
                                                "text": "@Museum Nama"
                                            },
                                            "color": "#3C80C1",
                                            "style": "primary"
                                        }
                                    ]
                                }
                            },
                            {
                                "type": "bubble",
                                "direction": "ltr",
                                "hero": {
                                    "type": "image",
                                    "url": "https://firebasestorage.googleapis.com/v0/b/digimu-31cdd.appspot.com/o/Chatbot_Digimu%2Fassets%2FCari%20Museum-2.png?alt=media&token=ac22d662-8044-4032-b342-5afafbb34c87",
                                    "flex": 1,
                                    "size": "full",
                                    "aspectRatio": "1.51:1",
                                    "aspectMode": "fit"
                                },
                                "body": {
                                    "type": "box",
                                    "layout": "vertical",
                                    "contents": [
                                        {
                                            "type": "text",
                                            "text": "Telusuri Museum Berdasarkan Wilayah Di Indonesia",
                                            "size": "lg",
                                            "align": "center",
                                            "wrap": true
                                        }
                                    ]
                                },
                                "footer": {
                                    "type": "box",
                                    "layout": "horizontal",
                                    "contents": [
                                        {
                                            "type": "button",
                                            "action": {
                                                "type": "message",
                                                "label": "Telusuri",
                                                "text": "@Museum Wilayah"
                                            },
                                            "color": "#3C80C1",
                                            "style": "primary",
                                            "gravity": "center"
                                        }
                                    ]
                                }
                            },
                            {
                                "type": "bubble",
                                "direction": "ltr",
                                "hero": {
                                    "type": "image",
                                    "url": "https://firebasestorage.googleapis.com/v0/b/digimu-31cdd.appspot.com/o/Chatbot_Digimu%2Fassets%2FCari%20Museum-2.png?alt=media&token=ac22d662-8044-4032-b342-5afafbb34c87",
                                    "size": "full",
                                    "aspectRatio": "1.51:1",
                                    "aspectMode": "fit"
                                },
                                "body": {
                                    "type": "box",
                                    "layout": "vertical",
                                    "contents": [
                                        {
                                            "type": "text",
                                            "text": "Temukan Museum Terfavorit Tiap Provinsi Di Indonesia",
                                            "size": "lg",
                                            "align": "center",
                                            "wrap": true
                                        }
                                    ]
                                },
                                "footer": {
                                    "type": "box",
                                    "layout": "horizontal",
                                    "contents": [
                                        {
                                            "type": "button",
                                            "action": {
                                                "type": "message",
                                                "label": "Telusuri",
                                                "text": "@Museum Terfavorit"
                                            },
                                            "color": "#3C80C1",
                                            "style": "primary"
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                }));
                break;
            default:
                console.log("Masuk Default");
                flagUmur = await dbRefB.get();
                if (flagUmur.data().fUmur === 1) {
                    console.log(message.text.toLowerCase());
                    await dbRefB.set({
                        'umur': message.text.toLowerCase(),
                        'fUmur': 0,
                    }, {merge: true});
                    await client.replyMessage(replyToken, [JSON.parse(JSON.stringify({
                        "type": "text",
                        "text": "Terimakasih atas informasinya 🤝🏻, selamat menggunakan bot Digimu semoga kepedulian kita terhadap museum dan kecintaan kita terhadap kebudayaan Indonesia dapat ditingkatkan ! ✊🏻"
                    })), imagemap_menu()]);
                } else {
                    await client.replyMessage(replyToken, imagemap_menu());
                }

        }
    } catch (e) {
        console.log("Ada error Handle Text", e);
    }
}

export async function handleLocation(message, replyToken, source, client, db) {
    const idUser = source.userId;
    let profile = await client.getProfile(idUser);
    try {
        const profileId = profile.userId;
        let getDoc = await db.collection('users').doc(profileId).get();

        const data = getDoc.data();
        const refDb = db.collection('users').doc(profileId);
        const refDbLokasiAwal = refDb.collection('lokasi').doc('lokasiAwal');
        const objectGantiLokasi = {
            'address': message.address.toString(),
            'latitude': message.latitude.toString(),
            'longitude': message.longitude.toString(),
        };
        const setFlagLocationZero = refDb.set({
            'fLocationAwal': 0
        }, {merge: true});

        if (data.fLocationAwal === 1) {
            await refDbLokasiAwal.set(objectGantiLokasi, {merge: true});
            await setFlagLocationZero;
            await client.replyMessage(replyToken, template_shareLokasi(message));
        } else if (data.fLocationGanti === 1) {
            await refDbLokasiAwal.set(objectGantiLokasi, {merge: true});
            await setFlagLocationZero;
            await client.replyMessage(replyToken, template_updateLokasi(message));
        }
    } catch (e) {
        console.log("Handle Location Error", e);
    }
}