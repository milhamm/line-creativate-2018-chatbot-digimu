import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyCYnfKazbLSZVOB2s_CPm5k_ZIVPtqoJ5E",
    authDomain: "digimu-31cdd.firebaseapp.com",
    databaseURL: "https://digimu-31cdd.firebaseio.com",
    projectId: "digimu-31cdd",
    storageBucket: "digimu-31cdd.appspot.com",
    messagingSenderId: "848538917828"
};
firebase.initializeApp(config);

export default firebase