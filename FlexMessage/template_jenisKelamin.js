export default function () {
    return JSON.parse(JSON.stringify({
        "type": "template",
        "altText": "Pilih Jenis Kelamin",
        "template": {
            "type": "carousel",
            "actions": [],
            "columns": [
                {
                    "thumbnailImageUrl": "https://firebasestorage.googleapis.com/v0/b/digimu-31cdd.appspot.com/o/Chatbot_Digimu%2Fassets%2FJenis%20Kelamin-03.jpg?alt=media&token=b3a4f7f7-06c9-4dbb-90d5-57a6d22fa95c",
                    "text": "Aku Cowok 👱🏻",
                    "actions": [
                        {
                            "type": "message",
                            "label": "Pilih",
                            "text": "Cowok"
                        }
                    ]
                },
                {
                    "thumbnailImageUrl": "https://firebasestorage.googleapis.com/v0/b/digimu-31cdd.appspot.com/o/Chatbot_Digimu%2Fassets%2FJenis%20Kelamin-04.jpg?alt=media&token=95ba45da-de2f-46ae-87af-dafd818eaa26",
                    "text": "Aku Cewek 👩🏻",
                    "actions": [
                        {
                            "type": "message",
                            "label": "Pilih",
                            "text": "Cewek"
                        }
                    ]
                }
            ]
        }
    }))
}