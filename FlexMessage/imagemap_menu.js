export default function () {
    return JSON.parse(JSON.stringify({
        "type": "imagemap",
        "baseUrl": "https://bukanavatar.com:1235/static/images/mainmenu",
        "altText": "Main Menu",
        "baseSize": {
            "width": 1040,
            "height": 701
        },
        "actions": [
            {
                "type": "message",
                "area": {
                    "x": 22,
                    "y": 164,
                    "width": 327,
                    "height": 247
                },
                "text": "Cari Museum"
            },
            {
                "type": "message",
                "area": {
                    "x": 24,
                    "y": 444,
                    "width": 328,
                    "height": 240
                },
                "text": "Pengetahuan Umum"
            },
            {
                "type": "message",
                "area": {
                    "x": 365,
                    "y": 440,
                    "width": 337,
                    "height": 246
                },
                "text": "Berita Museum"
            },
            {
                "type": "message",
                "area": {
                    "x": 363,
                    "y": 163,
                    "width": 339,
                    "height": 248
                },
                "text": "Museum Terdekat"
            },
            {
                "type": "message",
                "area": {
                    "x": 715,
                    "y": 166,
                    "width": 302,
                    "height": 242
                },
                "text": "Tiket Museum"
            },
            {
                "type": "message",
                "area": {
                    "x": 717,
                    "y": 440,
                    "width": 300,
                    "height": 245
                },
                "text": "Tentang Digimu"
            }
        ]
    }))
}