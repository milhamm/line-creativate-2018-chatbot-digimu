//Regular Events
import https from 'https';
import fs from 'fs';
import express from 'express';
import {Client, middleware} from '@line/bot-sdk';
import firebase from './Firebase';
//Events
import {follow} from './Events/events_follow';
import {handleLocation, handleText} from './Events/events_message';
import {handlePostback} from "./Events/events_postback";

const app = express();

const CHANNEL_ACCESS_TOKEN = '06Vef58BJwxS0Mzcaklz+xOQLBbFeZM2hq71pFKF00P7x9MGiRo/a9Y9CG9dIMaVDU/fAKbf3o3hG5bmUUW8kSDPHffNNPOyrqSVQBmdR2y9aYRBOezemvgq9e7haak05JKmM/j0dtrOeh67CYapYgdB04t89/1O/w1cDnyilFU=';
const CHANNEL_SECRET = 'b49606cc25f33131d9c762b8fbf753c6';
const config = {
    channelAccessToken: CHANNEL_ACCESS_TOKEN,
    channelSecret: CHANNEL_SECRET,
};
const client = new Client(config);

const db = firebase.firestore();
app.use('/static', express.static('assets'));
app.post('/callback', middleware(config), (req, res) => {
    res.writeHead(200);
    Promise
        .all(req.body.events.map(handleEvent))
        .then(result => {
            res.json(result);
            console.log(result);
        })
        .catch(err => {
            console.log(err);
            res.status(500).end();
        });
});

function handleEvent(event) {
    switch (event.type) {
        case 'follow':
            return follow(event.replyToken, event.source, client, db);
        case 'postback':
            return handlePostback(event.replyToken, event.source, event.postback, client, db);
        case 'message':
            const message = event.message;
            switch (message.type) {
                case 'text':
                    return handleText(message, event.replyToken, event.source, event.timestamp, client, db);
                case 'location':
                    return handleLocation(message, event.replyToken, event.source, client, db);
            }
    }
}

const options = {
    key: fs.readFileSync("/etc/letsencrypt/live/bukanavatar.com/privkey.pem"),
    cert: fs.readFileSync("/etc/letsencrypt/live/bukanavatar.com/fullchain.pem")
};

https.createServer(options, app).listen(1235);